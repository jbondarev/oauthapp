from flask import Flask


def create_app() -> Flask:
    app: Flask = Flask(__name__)
    return app


def register_blueprints() -> None:
    pass
